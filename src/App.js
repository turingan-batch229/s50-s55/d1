import {useState, useEffect} from 'react';
import './App.css';
// AppNavbar Importation
import AppNavbar from './components/AppNavbar.js';
import CourseView from './components/CourseView.js';
import {Container} from 'react-bootstrap/';
import{BrowserRouter as Router, Route, Routes} from 'react-router-dom/';
import Home from './Pages/Home.js';
import Courses from './Pages/Courses.js';
import Register from './Pages/Register.js';
import Login from './Pages/Login.js';
import Logout from './Pages/Logout.js';
import NotFound from './Pages/NotFound.js';
import {UserProvider} from './UserContext.js';


// React JS is a single page application (SPA)
// Whenever a link is clicked, it functions as if the page is being reloaded but what it actually does is it goes through the process of rendering, mounting, rerendering and unmounting components
// When a link is clicked, React JS changes the url of the application to mirror how HTML accesses its urls
// It renders the component executing the function component and it's expressions
// After rendering it mounts the component displaying the elements
// Whenever a state is updated or changes are made with React JS, it rerenders the component
// Lastly, when a different page is loaded, it unmounts the component and repeats this process
// The updating of the user interface closely mirrors that of how HTML deals with page navigation with the exception that React JS does not reload the whole page





function App() {

  const [user, setUser] = useState({

    /*
      Syntax:
      localStorage.getItem(propertyName)
    */

    // email: localStorage.getItem('email')

    id: null,
    isAdmin: null
  
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  })


  return (
    // Fragment "<> and </>"
  /*We are mounting our components and to prepare for output rendering*/
  <UserProvider value={{user, setUser, unsetUser}}>
   <> 
    <Router>
      <AppNavbar/>

      <Container>
        <Routes>
            <Route exact path="/" element={<Home/>}/>
            <Route exact path="/courses" element={<Courses/>}/>
            <Route exact path="/courseView/:courseId" element={<CourseView/>}/>
            <Route exact path="/login" element={<Login/>}/>
            <Route exact path="/logout" element={<Logout/>}/>
            <Route exact path="/register" element={<Register/>}/>
            <Route path='*' element={<NotFound/>}/>

        </Routes>
      </Container>        
    </Router>
  </>
  </UserProvider>
  );

}

export default App;
