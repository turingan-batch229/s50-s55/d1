import {Card, Button} from 'react-bootstrap';
import {useState} from 'react';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProps}){
	// Checks to see if the data was successfully passed
	console.log(courseProps);
	console.log(typeof courseProps);

	// Desturcturing the data to avoid dot notation
	const {name, description, price, _id} = courseProps;


	// 3 Hooks in React
	// 1. useState
	// 2. useEffect
	// 3. useContext 

	// Use the useState hook for the component to be able to store state
	// States are used to keep track of information related to individual components
	// Syntax -> const[getter, setter] = useState(initialGetterValue);

	const[count, setCount] = useState(0);
	const[seat, setSeat] = useState(30);
	console.log(useState(0));
	

	
/*	function enroll(){
		
			if(seat > 0){
				setCount(count + 1);
				console.log("Enrollees " + count);
				setSeat(seat - 1);
				console.log("Seats " + count);
			}else{
				alert("No more seats available!");
			}
		*/




		/*if(seat === 0){
			alert("No more seats!")
		}else{
			setCount(count + 1);
			console.log("Enrollees " + count);
			setSeat(seat - 1);
		}
		
	}*/



	return(
	
	
		<Card className="m-4">
	        <Card.Body>
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>{price}</Card.Text>
	            <Card.Text>{count} Enrollees</Card.Text>
	            <Link className="btn btn-primary" to={`/courseView/${_id}`}>Details</Link>
	            {/*<Button onClick={enroll} variant="primary">Enroll</Button>*/}
	        </Card.Body>
	    </Card>

		)
}







// My solution

	// <Card className="mx-4 px-2">
	// 	 <Card.Title className="px-3 mb-0 pt-3 pb-0">
    //         <h4>React JS</h4>
    //     </Card.Title >
	// 		<Card.Body className="pt-0 m-0">
             
    //     	<h5 className="pb-0 m-0">Description:</h5>
    //     		<p>Learn how to create your own website using ReactJS.</p>
    //     	<h5 className="pb-0 m-0">Price:</h5>
    //     		<p>PhP 10,000</p>
        
    //     <Button variant = "primary">Enroll</Button>
        
    // </Card.Body>
	// 			</Card>