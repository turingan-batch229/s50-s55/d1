// import Courses from './Pages/Courses.js';
import { useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';
	
export default function CourseView(){

	// Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	const navigate = useNavigate();

	const {user} = useContext(UserContext);

	// The "useParams" hook allows us to retrive the courseId passed via URL
	const{courseId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	useEffect(() => {
		console.log(courseId);

		fetch(`${ process.env.REACT_APP_API_URL }/courses/${courseId}`)
				.then(res => res.json())
				.then(data => {

					console.log(data);

					setName(data.name);
					setDescription(data.description);
					setPrice(data.price);

				});


	},[])

	const enroll = (courseId) => {
		fetch(`${ process.env.REACT_APP_API_URL }/users/enroll`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						Authorization: `Bearer ${ localStorage.getItem('token') }`
					},
					body: JSON.stringify({
						courseId: courseId
					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					if(data === true){
						Swal.fire({
							title: "Successfully Enrolled!",
							icon: 'success',
							text: "You have successfully enrolled for this course."
						})

			// The "push" method allows us to redirect the user to a different page and is an easier approach rather than using the "Redirect" component
						navigate("/courses");

					}else{
						Swal.fire({
							Title: "Something went wrong!",
							icon: 'error',
							text: "Please try again."
						})
					}

				});
	}

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>8 am - 5 pm</Card.Text>
							{
								(user.id !== null) ?
								<Button onClick={() =>enroll(courseId)} variant="primary" block>Enroll</Button>
								:
								<Link className="btn btn-danger btn-block" to="/login">Login</Link>

							}
							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
