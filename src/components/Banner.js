// Destructuring components/modules for cleaner codebase.
import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner({data}){

	console.log(data)
	const {title, content, destination, label} = data;

	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>{title}</h1>
				<p>{content}</p>
				<Button variant="primary" as={Link} to={destination}>{label}</Button>
			</Col>
		</Row>
		)
}


/*import {Button, Row, Col} from 'react-bootstrap';


export default function Banner(){
	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Oppportunities for everyone, everywhere!</p>
				<Button variant = "primary">Enroll Now!</Button>

			</Col>
		</Row>

		)
}*/