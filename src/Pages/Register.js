import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import {useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';


export default	function Register(){

	const navigate = useNavigate();

	const {user} = useContext(UserContext);

	// State Hooks - > store values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [fName, setFname] = useState('');
	const [lName, setLname] = useState('');
	const [mobNo, setMobNo] = useState('');

	const [isActive, setIsActive] = useState(false);


/*	console.log(email);
	console.log(password1);
	console.log(password2);
	console.log(fName);
	console.log(lName);
	console.log(mobNo);*/


	useEffect(() =>{
		// Validation to enable register button when all fields are populated and both password match
		if((email !== '' && password1 !== '' && password2 !== '' && fName !== '' && lName !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [email, password1, password2, fName, lName, mobNo])

	
	const registerUser = (e) => {
		e.preventDefault();
		fetch(`${ process.env.REACT_APP_API_URL }/users/checkEmail`, {
		method: "POST",
		headers: {
						"Content-Type": "application/json",
					},
		body: JSON.stringify({
			email: email
		})
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		if(data === false){
			fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				firstName: fName,
				lastName: lName,
				email: email,
				mobileNo: mobNo,
				password: password1,
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Successfully Registered",
					icon: 'success',
					text: `You have successfully registered ${email}.`
				})

				navigate("/login");

			}else{
				Swal.fire({
					title: "Something went wrong!",
					icon: 'error',
					text: "Please try again."
				})
			}

		});

	}else{
		Swal.fire({
					title: "Something went wrong!",
					icon: 'error',
					text: "Email already exists!"
				})
	}

})

	setEmail('');
	setPassword1('');
	setPassword2('');
	setFname('');
	setLname('');
	setMobNo('');

}

	

	return(

		(user.id !== null) ?

		<Navigate to="/courses"/>
		:

		<Form onSubmit={(e) => registerUser(e)}>
		  <Form.Group className="mb-3" controlId="userEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="password1">
		    <Form.Label>Password</Form.Label>
		    <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required />
		  </Form.Group>

		   <Form.Group className="mb-3" controlId="password2">
		    <Form.Label>Verify Password</Form.Label>
		    <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
		  </Form.Group>


		   <Form.Group className="mb-3" controlId="firstName">
		    <Form.Label>First Name</Form.Label>
		    <Form.Control type="text" placeholder="First Name" value={fName} onChange={e => setFname(e.target.value)} required/>
		  </Form.Group>


		   <Form.Group className="mb-3" controlId="lastName">
		    <Form.Label>Last Name</Form.Label>
		    <Form.Control type="text" placeholder="Last Name" value={lName} onChange={e => setLname(e.target.value)} required/>
		  </Form.Group>


		   <Form.Group className="mb-3" controlId="mobNo">
		    <Form.Label>Mobile No.</Form.Label>
		    <Form.Control type="number" placeholder="Mobile No" value={mobNo} onChange={e => setMobNo(e.target.value)} required/>
		  </Form.Group>



	{/*	  <Form.Group className="mb-3" controlId="formBasicCheckbox">
		    <Form.Check type="checkbox" label="Check me out" />
		  </Form.Group>*/}

		  {/*CONDITIONAL RENDERING -> IF ACTIVE BUTTON IS CLICKABLE -> IF INACTIVE BUTTON IS CLICKABLE*/}
	{
		(isActive) ? 
	  	<Button variant="primary" type="submit" controlId="submitBtn" to="/login">Register
	  	</Button>
	  	:
	  		<Button variant="primary" type="submit" controlId="submitBtn" disabled>
	  	Register
	  	</Button>
	  		}
	  	</Form>
		)
}

