import Banner from '../components/Banner';

export default function NotFound() {

    const data = {
        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }
    
    return (
        <Banner data={data}/>
    )
}





/*import { Link } from "react-router-dom";

export default function NotFound() {
	return (
		<div className="text-center">
			<h1>Page not found!</h1>
				<p>Please go back to <Link to='/'>Homepage</Link>.</p>
				
		</div>
	)
}*/