import Banner from '../components/Banner.js';
import Highligths from '../components/Highlights.js';

const data = {
	title: "Zuitt Coding Bootcamp",
	content: "Opportunities for everyone, everywhere",
	destination: "/courses",
	label: "Enroll Now"
}

export default function Home() {
	return(
		<>
			<Banner data={data}/>
	      	<Highligths/>
      	</>
		)
}





/*import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';


export default function Home() {
	return(
		<>
			<Banner/>   
      		<Highlights/> 

		</>

		)
}*/