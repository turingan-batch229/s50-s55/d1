import {useContext, useEffect} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

export	default function Logout(){

	// Consume the UserContext object and destructure it to access the user state and unsetUset function from the context provider
	const  {unsetUser, setUser} = useContext(UserContext);

	// Clear the localStorage of the user's information
	unsetUser();

	//localStorage.clear();


	useEffect(() => {

		// Allows us to set the user state back to its original value.
		setUser({id: null})
	})


	return(
			<Navigate to="/login"/>
		)
}